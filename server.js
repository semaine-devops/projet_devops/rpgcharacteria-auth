const express = require('express');
const app = express();
const admin = require('firebase-admin');

const axios = require('axios');



const serviceAccount = require('./voiture-coding-firebase-adminsdk-6t0qc-9ada210a3c.json');
require('dotenv').config()
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
})

const bodyParser = require("body-parser")
const {getAuth} = require("firebase-admin/auth");
const {auth} = require("firebase-admin");
const jwt = require('jsonwebtoken');
var cors = require('cors')

// Content-type: application/json
app.use(bodyParser.json())
app.use(cors())

// Content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}))
app.post("/signup", async (req, res) => {
    console.log('signup')
    product = {
        email: req.body.email,
        password: req.body.password
    }
    const userResponce = await admin.auth().createUser({
        email: product.email,
        password: product.password,
    });


    const secret = process.env.SECRET
    const token = jwt.sign({userId:userResponce.uid},secret);
    //envoyer le token a l'utilisateur
    console.log(token)


// Données de l'utilisateur
    const userData = {
        email: product.email,
        password: product.password,
    };

// Requête HTTP POST pour envoyer les données de l'utilisateur à l'API
    console.log('Données de l\'utilisateur :', userData)
    try{
    axios.post('http://localhost:3004/createquota', userData)
        .then((response) => {
            console.log('Réponse de l\'API :', response.data);
        })
        .catch((error) => {
            console.error('Erreur de la requête :', error);
        });

    
    res.json({jwt:token});
    }
    catch (e){
        console.log(e.toString())
    }
});

app.post("/login", async (req, res) => {
    console.log("login")
    let product = {
        email: req.body.email,
        password: req.body.password
    }

    //recuperer l'email et le mot de passe pour la connexion de l'utilisateur grace a firebase
    const userResponce = await admin.auth().getUserByEmail(product.email);

    //recuperer l'id de l'utilisateur
    const uid = userResponce.uid;
    auth.tenantId = uid;

    //recuperer le mot de passe de l'utilisateur
    const password = userResponce.passwordHash;

    //verifier si le mot de passe est correct
    const passwordCorrect = product.password.hash === password;

    //si le mot de passe est correct
    if (passwordCorrect) {
        //recuperer le token de l'utilisateur
        const secret = process.env.SECRET
        const token = jwt.sign({userId:userResponce.uid},secret);

        //envoyer le token a l'utilisateur
        console.log(token)
        res.json({jwt:token});
    } else {
        res.json("mot de passe incorrect");
    }

});

//supprimer un utilisateur
app.delete("/delete", async (req, res) => {
    product = {
        email: req.body.email,
    }
    const userResponce = await admin.auth().getUserByEmail(product.email);
    const uid = userResponce.uid;
    console.log(uid)
    admin.auth().deleteUser(uid)
        .then(function () {
            console.log("Successfully deleted user");
            res.json("Successfully deleted user");
        })
        .catch(function (error) {
            console.log("Error deleting user:", error);
        });
});

app.listen(3001, () => {
    console.log("Serveur démarré sur le port 3001");
});
